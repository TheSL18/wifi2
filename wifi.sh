#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Debe ser el usuario 'root' para utilizar esta utilidad"
  exit
fi


function ctrl_c(){
    echo -e "\n[!] Saliendo...\n"
    exit 1
}

#Ctrl+C
trap ctrl_c INT

echo "[+] Script para conectar a las wifi"
echo "[+] Por favor escoge una opcion y pulsa enter para continuar o pulsa Ctrl-C para salir:"
echo "-------------------------------------------------------------------------------------------"

echo "[+] Presione 1 coenctar a Mercusys "
echo "[+] Presione 2 para borrar messages "
#echo "[+] Presione 3 para mover el archivo parasito a /opt/remote (Solo sensor Mi Banco) "
#echo "[+] Presione 4 para descaragar nuevas veriones del repositorio "
#echo "[+] Presione 5 para crear la sincronizacion de hora con servidores seguros"
#echo "[+] Presione 6 para actualizar repositorios"
#echo "[+] Preciona 7 para solucionar los errores de Search LAG en los sensores que lo tengan"
#echo "[+] Presione 99 para ver la version del Script "

echo "[+] Ingresa una opción: "
# Lee la variable ingresada para acceder al menú
read opcion

# Usa case para encontrar un match en el menu de opciones y proceder con la accion
case $opcion in

# Opción 1
1)
echo "[+] Coenctando a mercusys"
echo "-------------------------------------------------------------------------------------------"
ifconfig wlan0 up
iwconfig wlan0 essid "MERCUSYS_254C_5GHz" key "1014307444"
dhclient wlan0
echo "[+] completo"
sleep 2
bash wifi.sh;


# Opción 2
2)
echo "[-] Borrando messages"
echo "-------------------------------------------------------------------------------------------"
echo "[!]"; rm -v /var/log/messages
echo "[+] completo"
sleep 2
bash wifi.sh;
